-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2020 at 05:11 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lora`
--
CREATE DATABASE IF NOT EXISTS `lora` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `lora`;

-- --------------------------------------------------------

--
-- Table structure for table `debits`
--

CREATE TABLE `debits` (
  `id` int(10) UNSIGNED NOT NULL,
  `debit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'karyawan', NULL, NULL),
(3, 'user', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_12_154812_create_debits_table', 1),
(5, '2019_11_12_162907_create_pumps_table', 1),
(6, '2019_11_13_160912_create_jabatans_table', 1),
(7, '2019_11_13_160955_create_waters_table', 1),
(15, '2020_03_30_135819_create_monitors_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `monitors`
--

CREATE TABLE `monitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_node` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `monitors`
--

INSERT INTO `monitors` (`id`, `id_node`, `mode`, `valve1`, `valve2`, `valve3`, `valve4`, `valve5`, `valve6`, `valve7`, `valve8`, `valve9`, `valve10`, `created_at`, `updated_at`) VALUES
(1, '1', '0', '1', '1', '0', '0', '1', '1', '0', '0', '1', '0', '2020-04-14 16:00:00', '2020-04-02 00:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pumps`
--

CREATE TABLE `pumps` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `jabatan`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$jFxH2EgGPM/wxLHC4lI8RujZK7ag6n7dtM8gquiTtGqcPbyLeH3Ne', 'admin', NULL, NULL, NULL),
(2, 'karyawan', 'karyawan@gmail.com', NULL, '$2y$10$.N4yLBe24V8ydH5NjDT4re5OlWkH/zLLNwYYRDnQbPUv6HFyO0p3e', 'karyawan', NULL, NULL, NULL),
(3, 'user', 'user@gmail.com', NULL, '$2y$10$jFR6qaeNfMl6AxSceN7WvugpfZjpMAT6.bnpHoYD.QAfvn4gGaH3i', 'user', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `waters`
--

CREATE TABLE `waters` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_node` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `battery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consumption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `waters`
--

INSERT INTO `waters` (`id`, `id_node`, `id_message`, `battery`, `valve`, `level`, `consumption`, `created_at`, `updated_at`) VALUES
(2, '1', '2', '12V', '1', '1', '11', '2020-02-24 00:40:45', '2020-02-24 00:40:45'),
(3, '1', '1', '12V', '1', '50', '22', '2020-04-01 23:49:46', '2020-04-01 23:49:46'),
(347, '11', '122', '12', '3', '3', '0', '2020-04-02 00:58:51', '2020-04-02 00:58:51'),
(348, '11', '123', '12', '3', '3', '0', '2020-04-02 00:58:59', '2020-04-02 00:58:59'),
(349, '11', '124', '12', '3', '3', '0', '2020-04-02 00:59:02', '2020-04-02 00:59:02'),
(350, '11', '125', '12', '3', '3', '0', '2020-04-02 00:59:10', '2020-04-02 00:59:10'),
(351, '11', '126', '12', '3', '3', '0', '2020-04-02 00:59:22', '2020-04-02 00:59:22'),
(352, '11', '127', '12', '3', '3', '0', '2020-04-02 00:59:27', '2020-04-02 00:59:27'),
(353, '11', '128', '12', '3', '3', '0', '2020-04-02 00:59:37', '2020-04-02 00:59:37'),
(354, '11', '129', '12', '3', '3', '0', '2020-04-02 00:59:44', '2020-04-02 00:59:44'),
(355, '11', '130', '12', '3', '3', '0', '2020-04-02 00:59:51', '2020-04-02 00:59:51'),
(356, '11', '131', '12', '3', '3', '0', '2020-04-02 00:59:57', '2020-04-02 00:59:57'),
(357, '11', '132', '12', '3', '3', '0', '2020-04-02 01:00:03', '2020-04-02 01:00:03'),
(358, '11', '133', '12', '3', '3', '0', '2020-04-02 01:00:14', '2020-04-02 01:00:14'),
(359, '11', '134', '12', '3', '3', '0', '2020-04-02 01:00:18', '2020-04-02 01:00:18'),
(360, '11', '135', '12', '3', '3', '0', '2020-04-02 01:00:25', '2020-04-02 01:00:25'),
(361, '11', '136', '12', '3', '3', '0', '2020-04-02 01:00:32', '2020-04-02 01:00:32'),
(362, '11', '137', '12', '3', '3', '0', '2020-04-02 01:00:38', '2020-04-02 01:00:38'),
(363, '11', '138', '12', '3', '3', '0', '2020-04-02 01:00:45', '2020-04-02 01:00:45'),
(364, '11', '139', '12', '3', '3', '0', '2020-04-02 01:00:51', '2020-04-02 01:00:51'),
(365, '14', '132', '12', '67', '0', '0', '2020-04-02 01:01:03', '2020-04-02 01:01:03'),
(366, '11', '141', '12', '3', '3', '0', '2020-04-02 01:01:10', '2020-04-02 01:01:10'),
(367, '11', '142', '12', '3', '3', '0', '2020-04-02 01:01:13', '2020-04-02 01:01:13'),
(368, '11', '143', '12', '3', '3', '0', '2020-04-02 01:01:25', '2020-04-02 01:01:25'),
(369, '11', '144', '12', '3', '3', '0', '2020-04-02 01:01:34', '2020-04-02 01:01:34'),
(370, '11', '145', '12', '3', '3', '0', '2020-04-02 01:01:44', '2020-04-02 01:01:44'),
(371, '11', '146', '12', '3', '3', '0', '2020-04-02 01:01:50', '2020-04-02 01:01:50'),
(372, '11', '147', '12', '3', '3', '0', '2020-04-02 01:01:57', '2020-04-02 01:01:57'),
(373, '11', '148', '12', '3', '3', '0', '2020-04-02 01:02:08', '2020-04-02 01:02:08'),
(374, '11', '149', '12', '3', '3', '0', '2020-04-02 01:02:10', '2020-04-02 01:02:10'),
(375, '11', '150', '12', '3', '3', '0', '2020-04-02 01:02:16', '2020-04-02 01:02:16'),
(376, '11', '151', '12', '3', '3', '0', '2020-04-02 01:02:23', '2020-04-02 01:02:23'),
(377, '11', '152', '12', '3', '3', '0', '2020-04-02 01:02:27', '2020-04-02 01:02:27'),
(378, '11', '153', '12', '3', '3', '0', '2020-04-02 01:02:30', '2020-04-02 01:02:30'),
(379, '11', '154', '12', '3', '3', '0', '2020-04-02 01:02:39', '2020-04-02 01:02:39'),
(380, '11', '155', '12', '3', '3', '0', '2020-04-02 01:02:50', '2020-04-02 01:02:50'),
(381, '11', '156', '12', '3', '3', '0', '2020-04-02 01:02:53', '2020-04-02 01:02:53'),
(382, '11', '157', '12', '3', '3', '0', '2020-04-02 01:02:59', '2020-04-02 01:02:59'),
(383, '11', '158', '12', '3', '3', '0', '2020-04-02 01:03:10', '2020-04-02 01:03:10'),
(384, '11', '159', '12', '3', '3', '0', '2020-04-02 01:03:22', '2020-04-02 01:03:22'),
(385, '11', '160', '12', '3', '3', '0', '2020-04-02 01:03:26', '2020-04-02 01:03:26'),
(386, '11', '161', '12', '3', '3', '0', '2020-04-02 01:03:30', '2020-04-02 01:03:30'),
(387, '11', '162', '12', '3', '3', '0', '2020-04-02 01:03:36', '2020-04-02 01:03:36'),
(388, '11', '163', '12', '3', '3', '0', '2020-04-02 01:03:43', '2020-04-02 01:03:43'),
(389, '11', '164', '12', '3', '3', '0', '2020-04-02 01:03:51', '2020-04-02 01:03:51'),
(390, '11', '165', '12', '3', '3', '0', '2020-04-02 01:03:56', '2020-04-02 01:03:56'),
(391, '11', '166', '140', '3', '3', '0', '2020-04-02 01:04:04', '2020-04-02 01:04:04'),
(392, '11', '167', '12', '3', '3', '0', '2020-04-02 01:04:12', '2020-04-02 01:04:12'),
(393, '11', '169', '12', '0', '3', '0', '2020-04-02 01:04:15', '2020-04-02 01:04:15'),
(394, '11', '171', '12', '3', '3', '0', '2020-04-02 01:04:27', '2020-04-02 01:04:27'),
(395, '11', '172', '12', '3', '3', '0', '2020-04-02 01:04:31', '2020-04-02 01:04:31'),
(396, '11', '173', '12', '3', '3', '0', '2020-04-02 01:04:36', '2020-04-02 01:04:36'),
(397, '11', '174', '12', '3', '3', '0', '2020-04-02 01:04:42', '2020-04-02 01:04:42'),
(398, '11', '175', '12', '3', '3', '0', '2020-04-02 01:04:47', '2020-04-02 01:04:47'),
(399, '11', '176', '12', '3', '3', '0', '2020-04-02 01:04:52', '2020-04-02 01:04:52'),
(400, '11', '177', '12', '3', '3', '0', '2020-04-02 01:04:54', '2020-04-02 01:04:54'),
(401, '11', '178', '12', '3', '3', '0', '2020-04-02 01:05:04', '2020-04-02 01:05:04'),
(402, '11', '179', '12', '3', '3', '0', '2020-04-02 01:05:06', '2020-04-02 01:05:06'),
(403, '11', '180', '12', '3', '3', '0', '2020-04-02 01:05:16', '2020-04-02 01:05:16'),
(404, '11', '181', '12', '3', '3', '0', '2020-04-02 01:05:23', '2020-04-02 01:05:23'),
(405, '11', '182', '12', '3', '3', '0', '2020-04-02 01:05:26', '2020-04-02 01:05:26'),
(406, '11', '183', '12', '3', '3', '0', '2020-04-02 01:05:28', '2020-04-02 01:05:28'),
(407, '11', '184', '12', '3', '3', '0', '2020-04-02 01:05:39', '2020-04-02 01:05:39'),
(408, '11', '185', '12', '3', '3', '0', '2020-04-02 01:05:48', '2020-04-02 01:05:48'),
(409, '11', '186', '12', '3', '3', '0', '2020-04-02 01:05:57', '2020-04-02 01:05:57'),
(410, '11', '189', '12', '3', '3', '0', '2020-04-02 01:06:21', '2020-04-02 01:06:21'),
(411, '11', '190', '12', '3', '3', '0', '2020-04-02 01:06:30', '2020-04-02 01:06:30'),
(412, '11', '191', '12', '3', '3', '0', '2020-04-02 01:06:40', '2020-04-02 01:06:40'),
(413, '11', '192', '12', '3', '3', '0', '2020-04-02 01:06:52', '2020-04-02 01:06:52'),
(414, '11', '193', '12', '3', '3', '0', '2020-04-02 01:06:59', '2020-04-02 01:06:59'),
(415, '11', '194', '12', '3', '3', '0', '2020-04-02 01:07:11', '2020-04-02 01:07:11'),
(416, '11', '195', '12', '3', '3', '0', '2020-04-02 01:07:15', '2020-04-02 01:07:15'),
(417, '11', '196', '12', '3', '0', '0', '2020-04-02 01:07:25', '2020-04-02 01:07:25'),
(418, '11', '197', '12', '3', '3', '0', '2020-04-02 01:07:32', '2020-04-02 01:07:32'),
(419, '11', '198', '12', '3', '3', '0', '2020-04-02 01:07:34', '2020-04-02 01:07:34'),
(420, '11', '199', '12', '3', '3', '0', '2020-04-02 01:07:46', '2020-04-02 01:07:46'),
(421, '11', '200', '12', '3', '3', '0', '2020-04-02 01:07:57', '2020-04-02 01:07:57'),
(422, '11', '201', '12', '3', '3', '0', '2020-04-02 01:08:00', '2020-04-02 01:08:00'),
(423, '11', '202', '12', '3', '3', '0', '2020-04-02 01:08:05', '2020-04-02 01:08:05'),
(424, '11', '203', '12', '3', '3', '0', '2020-04-02 01:08:16', '2020-04-02 01:08:16'),
(425, '11', '204', '12', '3', '3', '0', '2020-04-02 01:08:20', '2020-04-02 01:08:20'),
(426, '11', '205', '12', '3', '3', '0', '2020-04-02 01:08:24', '2020-04-02 01:08:24'),
(427, '11', '206', '12', '3', '3', '0', '2020-04-02 01:08:31', '2020-04-02 01:08:31'),
(428, '11', '207', '12', '3', '3', '0', '2020-04-02 01:08:37', '2020-04-02 01:08:37'),
(429, '11', '208', '12', '3', '3', '0', '2020-04-02 01:08:43', '2020-04-02 01:08:43'),
(430, '11', '209', '12', '3', '3', '0', '2020-04-02 01:08:47', '2020-04-02 01:08:47'),
(431, '11', '210', '12', '3', '3', '0', '2020-04-02 01:08:54', '2020-04-02 01:08:54'),
(432, '11', '211', '12', '3', '3', '0', '2020-04-02 01:09:05', '2020-04-02 01:09:05'),
(433, '11', '212', '12', '3', '3', '0', '2020-04-02 01:09:09', '2020-04-02 01:09:09'),
(434, '11', '213', '12', '3', '3', '0', '2020-04-02 01:09:15', '2020-04-02 01:09:15'),
(435, '11', '214', '12', '3', '3', '0', '2020-04-02 01:09:23', '2020-04-02 01:09:23'),
(436, '11', '215', '12', '3', '3', '0', '2020-04-02 01:09:34', '2020-04-02 01:09:34'),
(437, '11', '216', '12', '3', '3', '0', '2020-04-02 01:09:38', '2020-04-02 01:09:38'),
(438, '11', '217', '12', '3', '3', '0', '2020-04-02 01:09:41', '2020-04-02 01:09:41'),
(439, '11', '218', '12', '3', '3', '0', '2020-04-02 01:09:43', '2020-04-02 01:09:43'),
(440, '11', '219', '12', '3', '3', '0', '2020-04-02 01:09:55', '2020-04-02 01:09:55'),
(441, '11', '220', '12', '3', '3', '0', '2020-04-02 01:09:58', '2020-04-02 01:09:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `debits`
--
ALTER TABLE `debits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monitors`
--
ALTER TABLE `monitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pumps`
--
ALTER TABLE `pumps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `waters`
--
ALTER TABLE `waters`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `debits`
--
ALTER TABLE `debits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `monitors`
--
ALTER TABLE `monitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pumps`
--
ALTER TABLE `pumps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `waters`
--
ALTER TABLE `waters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=442;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
