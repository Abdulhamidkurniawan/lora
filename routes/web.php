<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();

// Route::middleware('cors')->group(function() {
  // Route::post('/loginapps', function(){ return 'sukses'; });
// });
// Route::post('/loginapps', 'Auth\LoginController@login')->name('loginapps');
// Route::post('/logoutapps', 'Auth\LoginController@logout')->name('logoutapps');

Route::get('/', 'RedirectController@default')->name('default');
Route::get('/water/get/{water}', 'WaterController@get')->name('water.get');
Route::get('/water/gets', 'WaterController@gets')->name('water.gets');

Route::middleware('auth')->group(function() {

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/dashboard/power_dashboard', 'PowerController@dashboard')->name('power.dashboard');
Route::get('/power', 'PowerController@index')->name('power.index');
Route::post('/power/cari', 'PowerController@cari')->name('power.cari');
Route::get('/dashboard/water_dashboard', 'WaterController@dashboard')->name('water.dashboard');
Route::get('/water', 'WaterController@index')->name('water.index');
Route::get('/optimize', 'HomeController@optimize')->name('optimize');

Route::middleware('admin')->group(function() {
    // Ruoute akun user
    Route::get('/user_panel', 'UserPanelController@index')->name('user_panel.index');
    Route::get('/user_panel/create', 'UserPanelController@create')->name('user_panel.create');
    Route::post('/user_panel/create', 'UserPanelController@store')->name('user_panel.store');
    // Route::get('/user_panel/{post}', 'UserPanelController@show')->name('user_panel.show');
    Route::get('/user_panel/{user}/edit', 'UserPanelController@edit')->name('user_panel.edit');
    Route::patch('/user_panel/{user}/edit', 'UserPanelController@update')->name('user_panel.update');
    Route::delete('/user_panel/{user}/delete', 'UserPanelController@destroy')->name('user_panel.destroy');

    Route::get('/water/create', 'WaterController@create')->name('water.create');
    Route::post('/water/create', 'WaterController@store')->name('water.store');
    Route::get('/water/{water}/edit', 'WaterController@edit')->name('water.edit');
    Route::patch('/water/{water}/edit', 'WaterController@update')->name('water.update');
    Route::delete('/water/{water}/delete', 'WaterController@destroy')->name('water.destroy');

    Route::get('/monitor', 'MonitorController@index')->name('monitor.index');
    Route::get('/monitor/create', 'MonitorController@create')->name('monitor.create');
    Route::post('/monitor/create', 'MonitorController@store')->name('monitor.store');
    Route::get('/monitor/{monitor}/controlon', 'MonitorController@controlon')->name('monitor.controlon');
    Route::get('/monitor/{monitor}/controloff', 'MonitorController@controloff')->name('monitor.controloff');
    Route::get('/monitor/{monitor}/edit', 'MonitorController@edit')->name('monitor.edit');
    Route::patch('/monitor/{monitor}/edit', 'MonitorController@update')->name('monitor.update');
    Route::delete('/monitor/{monitor}/delete', 'MonitorController@destroy')->name('monitor.destroy');

    Route::get('/node_monitor', 'PowerController@node')->name('power.node');
    Route::get('/usage_monitor', 'PowerController@usage')->name('power.usage');
    Route::post('/usage_monitor_report', 'PowerController@usage_report')->name('power.usage_report');
    Route::post('/report', 'PowerController@report')->name('power.report');
    Route::post('/phase_monitor', 'PowerController@phase')->name('power.phase_monitor');

    Route::delete('/power/{power}/delete', 'PowerController@destroy')->name('power.destroy');

    Route::get('/resources', 'ResourcesController@index')->name('resources.index');
    Route::get('/resources/setting', 'ResourcesController@setting')->name('resources.setting');
    Route::post('/resources', 'ResourcesController@index')->name('resources.post');
    Route::get('/absresourcesen/create', 'ResourcesController@create')->name('resources.create');
    Route::post('/resources/create', 'ResourcesController@store')->name('resources.store');
    Route::get('/resources/{resources}/edit', 'ResourcesController@edit')->name('resources.edit');
    Route::patch('/resources/{resources}/edit', 'ResourcesController@update')->name('resources.update');
    Route::delete('/resources/{resources}/delete', 'ResourcesController@destroy')->name('resources.destroy');
    });});
