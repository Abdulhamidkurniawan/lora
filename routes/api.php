<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/water', 'ApiWaterController@index');
Route::post('/water', 'ApiWaterController@create');
Route::get('/water/{water}', 'ApiWaterController@show');
Route::put('/water/{water}', 'ApiWaterController@update');
Route::delete('/water/{water}', 'ApiWaterController@destroy');

Route::get('/controlindex/{water}', 'ApiWaterController@controlindex');

Route::get('/power', 'ApiPowerController@index');
Route::post('/power', 'ApiPowerController@create');
Route::get('/power/{power}', 'ApiPowerController@show');
Route::put('/power', 'ApiPowerController@update');
Route::delete('/power/{power}', 'ApiPowerController@destroy');

Route::post('/loginapps', 'ApiUserPanelController@cari');

Route::get('/node', 'ApiPowerController@node');
Route::get('/data_node/{power}', 'ApiPowerController@data_node');

