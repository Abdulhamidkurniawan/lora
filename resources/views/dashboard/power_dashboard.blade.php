@extends('layouts.dashboard')
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
  $( function() {
  $( "#datepicker" ).datepicker({
    format: 'yyyy-mm-dd',
    submitFormat: 'yyyy-mm-dd'
});
  $( "#datepicker2" ).datepicker({
    format: 'yyyy-mm-dd',
    submitFormat: 'yyyy-mm-dd'
});
} );
});
// Code that uses other library's $ can follow here.
</script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="{{asset('js/jquery-dropdown-datepicker.min.js')}}"></script>
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
$("#date").dropdownDatepicker({
    defaultDateFormat: 'yyyy-mm-dd',
    displayFormat: 'dmy',
    minYear: 1940,
    allowFuture:false,
    submitFormat: 'yyyy-mm-dd'
});
});
</script>
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Power Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow" onclick="location.href='{{ route('power.node') }}';">
            <div class="inner">
              <h3>{{$nodep}}</h3>

              <p>Node Monitor</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{ route('power.node') }}" class="small-box-footer">Klik disini <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green" data-toggle="modal" data-target="#modalpower">
              <div class="inner">
                <h3>{{$valp}}</h3>

                <p>Power Monitor</p>
              </div>
              <div class="icon">
                <i class="fa fa-line-chart"></i>
              </div>
              <a href="#" class="small-box-footer" >Klik disini <i class="fa fa-arrow-circle-right"></i></a>

            </div>
          </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua" data-toggle="modal" data-target="#modalreport">
            <div class="inner">
              <h3>Usage</h3>

              <p>Report</p>
            </div>
            <div class="icon">
              <i class="ion ion-cloud"></i>
            </div>
            <a href="#" class="small-box-footer">Klik disini <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red" data-toggle="modal" data-target="#modalphase">
            <div class="inner">
              <h3>Phase</h3>

              <p>Monitor</p>
            </div>
            <div class="icon">
              <i class="ion ion-cloud"></i>
            </div>
            <a href="#" class="small-box-footer">Klik disini <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->

      <!-- /.row (main row) -->
      <div class="modal fade" id="modalpower" tabindex="-1" aria-labelledby="modalpower" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Penggunaan KWH</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
        </div>
        <div class="modal-body">
        <!--FORM TAMBAH BARANG-->
        <form method="POST" action="{{ route('power.usage_report') }}">
        @csrf

        <div class="form-group row">
        <label for="dari" class="col-md-4 col-form-label text-md-right" align="right">Node</label>
        <div class="col-md-4">
            <select name="node" class="form-control">
                <option value="">Pilih</option>
                @foreach ($nodes as $node)
                <option value="{{$node->id_node}}">Node - {{$node->id_node}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
        </div>
        </div>
        <div class="form-group row">
        <label for="dari" class="col-md-4 col-form-label text-md-right" align="right">Dari</label>
        <div class="col-md-4">
        <input id="" type="date" class="form-control" name="dari" value="{{ old('dari') }}" required autofocus>
        </div>
        <div class="col-md-2">
        </div>
        </div>
        <div class="form-group row">
        <label for="sampai" class="col-md-4 col-form-label text-md-right" align="right">Sampai</label>
        <div class="col-md-4">
        <input id="" type="date" class="form-control" name="sampai" value="{{ old('sampai') }}" required autofocus>
        </div>
        <div class="col-md-2">
        <button type="submit" class="btn btn-primary">Buka Data</button>
        </div>
        </div>
        </form>
        <!--END FORM TAMBAH BARANG-->
        </div>
        </div>
        </div>
        </div>

        <div class="modal fade" id="modalreport" tabindex="-1" aria-labelledby="modalreport" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Report KWH</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </button>
            </div>
            <div class="modal-body">
            <!--FORM TAMBAH BARANG-->
            <form method="POST" action="{{ route('power.report') }}">
            @csrf

            <div class="form-group row">
            <label for="dari" class="col-md-4 col-form-label text-md-right" align="right">Node</label>
            <div class="col-md-4">
                <select name="node" class="form-control">
                    <option value="">Pilih</option>
                    @foreach ($nodes as $node)
                    <option value="{{$node->id_node}}">Node - {{$node->id_node}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
            </div>
            </div>
            <div class="form-group row">
            <label for="dari" class="col-md-4 col-form-label text-md-right" align="right">Dari</label>
            <div class="col-md-4">
            <input id="" type="month" class="form-control" name="dari" value="{{ old('dari') }}" required autofocus>
            </div>
            <div class="col-md-2">
            </div>
            </div>
            <div class="form-group row">
            <label for="sampai" class="col-md-4 col-form-label text-md-right" align="right">Sampai</label>
            <div class="col-md-4">
            <input id="" type="month" class="form-control" name="sampai" value="{{ old('sampai') }}" required autofocus>
            </div>
            <div class="col-md-2">
            <button type="submit" class="btn btn-primary">Buka Data</button>
            </div>
            </div>
            </form>
            <!--END FORM TAMBAH BARANG-->
            </div>
            </div>
            </div>
            </div>
        <div class="modal fade" id="modalreport" tabindex="-1" aria-labelledby="modalreport" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Report KWH</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </button>
            </div>
            <div class="modal-body">
            <!--FORM TAMBAH BARANG-->
            <form method="POST" action="{{ route('power.report') }}">
            @csrf

            <div class="form-group row">
            <label for="dari" class="col-md-4 col-form-label text-md-right" align="right">Node</label>
            <div class="col-md-4">
                <select name="node" class="form-control">
                    <option value="">Pilih</option>
                    @foreach ($nodes as $node)
                    <option value="{{$node->id_node}}">Node - {{$node->id_node}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
            </div>
            </div>
            <div class="form-group row">
            <label for="dari" class="col-md-4 col-form-label text-md-right" align="right">Dari</label>
            <div class="col-md-4">
            <input id="" type="month" class="form-control" name="dari" value="{{ old('dari') }}" required autofocus>
            </div>
            <div class="col-md-2">
            </div>
            </div>
            <div class="form-group row">
            <label for="sampai" class="col-md-4 col-form-label text-md-right" align="right">Sampai</label>
            <div class="col-md-4">
            <input id="" type="month" class="form-control" name="sampai" value="{{ old('sampai') }}" required autofocus>
            </div>
            <div class="col-md-2">
            <button type="submit" class="btn btn-primary">Buka Data</button>
            </div>
            </div>
            </form>
            <!--END FORM TAMBAH BARANG-->
            </div>
            </div>
            </div>
            </div>

            <div class="modal fade" id="modalphase" tabindex="-1" aria-labelledby="modalphase" aria-hidden="true">
              <div class="modal-dialog">
              <div class="modal-content">
              <div class="modal-header">
              <h5 class="modal-title">Monitor Fasa</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              </button>
              </div>
              <div class="modal-body">
              <!--FORM TAMBAH BARANG-->
              <form method="POST" action="{{ route('power.phase_monitor') }}">
              @csrf
      
              <div class="form-group row">
              <label for="dari" class="col-md-4 col-form-label text-md-right" align="right">Node</label>
              <div class="col-md-4">
                  <select name="node" class="form-control">
                      <option value="">Pilih</option>
                      @foreach ($nodes as $node)
                      <option value="{{$node->id_node}}">Node - {{$node->id_node}}</option>
                      @endforeach
                  </select>
              </div>
              <div class="col-md-2">
              </div>
              </div>
              <div class="form-group row">
              <label for="dari" class="col-md-4 col-form-label text-md-right" align="right">Dari</label>
              <div class="col-md-4">
              <input id="" type="date" class="form-control" name="dari" value="{{ old('dari') }}" required autofocus>
              </div>
              <div class="col-md-2">
              </div>
              </div>
              <div class="form-group row">
              <label for="sampai" class="col-md-4 col-form-label text-md-right" align="right">Sampai</label>
              <div class="col-md-4">
              <input id="" type="date" class="form-control" name="sampai" value="{{ old('sampai') }}" required autofocus>
              </div>
              <div class="col-md-2">
              <button type="submit" class="btn btn-primary">Buka Data</button>
              </div>
              </div>
              </form>
              <!--END FORM TAMBAH BARANG-->
              </div>
              </div>
              </div>
              </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2021 <a href="#">Jurusan Teknik Elektro</a>
  </footer>

  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endsection
