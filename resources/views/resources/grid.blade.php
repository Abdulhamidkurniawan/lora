
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Advanced grid demo</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('node_modules/demo.css')}}" />

  <script type="module" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule="" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.js"></script>
  <!-- jQuery 3 -->
  <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
  <!-- support for IE -->
  <script src="{{asset('node_modules/gridstack/dist/es5/gridstack-poly.js')}}"></script>
  <script src="{{asset('node_modules/gridstack/dist/es5/gridstack-all.js')}}"></script>

  <style type="text/css">
    .grid-stack-item-removing {
      opacity: 0.8;
      filter: blur(5px);
    }
    #trash {
      background: rgba(255, 0, 0, 0.4);
    }
  </style>
</head>

<body>
  <h1>Advanced Demo</h1>
  <div class="row" style="padding: 5px; margin-bottom: 15px; border-style: ridge;">
    <div class="col-md-2 d-none d-md-block" style="padding: 5px; margin-bottom: 15px; border-style: ridge;">
      <div id="trash" style="padding: 5px; margin-bottom: 15px; border-style: ridge;" class="text-center ">
        <div>
          <ion-icon name="trash" style="font-size: 300%"></ion-icon>
        </div>
        <div>
          <span>Drop here to remove!</span>
        </div>
      </div>
      <div class="newWidget grid-stack-item">
        <div class="grid-stack-item-content" style="padding: 5px;">
          <div>
            <ion-icon name="add-circle" style="font-size: 300%"></ion-icon>
          </div>
          <div>
            <span>Drag me in the dashboard!</span>
          </div>
        </div>
      </div>
      <div class="newWidget grid-stack-item">
        <div class="grid-stack-item-content" style="padding: 5px;">
          <div>
            <ion-icon name="add-circle" style="font-size: 300%"></ion-icon>
          </div>
          <div>
            <span>Drag me in the dashboard!</span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-10">
      <div class="grid-stack" style="padding: 5px; margin-bottom: 15px; border-style: ridge;"></div>
    </div>
    <a class="btn btn-primary" onClick="save(true, false)" href="#">Save list</a>
  </div>

  <script type="text/javascript">
      jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    var user = {!! auth()->user() !!};
      let grid = GridStack.init({
      cellHeight: 70,
      minRow: 2,
      maxRow: 7,
      acceptWidgets: true,
      removable: '#trash', // drag-out delete class
      staticGrid: false
    });
    
    GridStack.setupDragIn('.newWidget', { appendTo: 'body', helper: 'clone' });

    let items = [
      // {x: 0, y: 0, w: 2, h: 2, id:user['email']+1, content: '1'},
      // {x: 4, y: 0, w: 4, h: 4, noMove: true, noResize: true, locked: true, content: 'I can\'t be moved or dragged!<br><ion-icon name="ios-lock" style="font-size:300%"></ion-icon>'},
      // {x: 8, y: 0, w: 2, h: 2, id:user['email']+1, content: '<p class="card-text text-center" style="margin-bottom: 0">Drag me!<p class="card-text text-center"style="margin-bottom: 0"><ion-icon name="hand" style="font-size: 300%"></ion-icon><p class="card-text text-center" style="margin-bottom: 0">...but don\'t resize me!'},
      // {x: 10, y: 0, w: 2, h: 2, content: '4'},
      // {x: 0, y: 2, w: 2, h: 2, content: '5'},
      // {x: 2, y: 2, w: 2, h: 4, content: '6'},
      // {x: 8, y: 2, w: 4, h: 2, content: '7'},
      // {x: 0, y: 4, w: 2, h: 2, content: '8'},
      // {x: 4, y: 4, w: 4, h: 2, content: '9'},
      // {x: 8, y: 4, w: 2, h: 2, content: '10'},
      // {x: 10, y: 4, w: 2, h: 2, content: '11'},
    ];
    grid.load(items);
    
    grid.on('added', function(e, items) {
      let str = '';
      items.forEach(function(item) { str += ' (x,y,w,h)=' + item.x + ',' + item.y + ',' + item.w + ',' + item.h + ' isi=' + item.content; });
      console.log(e.type + ' menambah' + ' ' + items.length + ' items:' + str );
    });

    grid.on('change', function(e, items) {
      let str = '';
      items.forEach(function(item) { str += ' (x,y,w,h)=' + item.x + ',' + item.y + ',' + item.w + ',' + item.h + ' isi=' + item.content; });
      console.log(e.type + ' mengubah' + ' ' + items.length + ' items:' + str );
    });

    grid.on('removed', function(e, items) {
      let str = '';
      items.forEach(function(item) { str += ' (x,y,w,h)=' + item.x + ',' + item.y + ',' + item.w + ',' + item.h + ' isi=' + item.content; });
      console.log(e.type + ' menghapus' + ' ' + items.length + ' items:' + str );
    });

    function save(content = true, full = true) {
      options = grid.save(content, full);
      // console.log(options);
      console.log(JSON.stringify(options));
      // alert ("ok save");
      $.ajax({
           type:'POST',
           url: "{{ route('resources.store') }}",
           data:{message:options},
           success:function(data){
              alert('Berhasil Simpan');
              console.log(options);
              console.log(data);
           }
        });
    }
  </script>
</body>

</html>