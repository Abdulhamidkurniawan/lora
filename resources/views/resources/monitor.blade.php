@extends('layouts.dashboard')
@section('content')



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Phase Monitor
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">

        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Small boxes (Stat box) -->
      <!-- <section class="content">
      <div class="row">
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Interactive Area Chart</h3>


            </div>
            <div class="box-body">
            <div id="chart"></div>
          </div>
          </div>

        </div> -->
      <div class="row">

<div class="box">

          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="row" style="padding: 5px; margin-bottom: 15px; border-style: ridge;">
              <div class="col-md-2 d-none d-md-block" style="padding: 5px; margin-bottom: 15px; border-style: ridge;">
                <div id="trash" style="padding: 5px; margin-bottom: 15px; border-style: ridge;" class="text-center ">
                  <div>
                    <ion-icon name="trash" style="font-size: 300%"></ion-icon>
                  </div>
                  <div>
                    <span>Drop here to remove!</span>
                  </div>
                </div>
                <div class="newWidget grid-stack-item" gs-w="2" gs-h="2">
                  <div class="grid-stack-item-content" style="padding: 5px; border-style: ridge;">
                    <div>
                      <ion-icon name="add-circle" style="font-size: 300%"></ion-icon>
                    </div>
                    <div>
                      <span>Drag me in the dashboard!</span>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-sm-12 col-md-10">
                <div class="grid-stack" style="padding: 5px; margin-bottom: 15px; border-style: ridge;"></div>
              </div>
              <a class="btn btn-primary" onClick="save(true, false)" href="#">Save list</a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
            <!-- /.box-header -->
            <div class="box-body">
            
            </div>
            <!-- /.box-body -->
          </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2021 <a href="#">Jurusan Teknik Elektro</a>
  </footer>

  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('node_modules/demo.css')}}" />
  <script src="{{asset('node_modules/gridstack/dist/es5/gridstack-poly.js')}}"></script>
  <script src="{{asset('node_modules/gridstack/dist/gridstack-all.js')}}"></script>
  <script type="module" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule="" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.js"></script>
  <script type="text/javascript">
    jQuery.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  var user = {!! auth()->user() !!};
    let grid = GridStack.init({
    cellHeight: 70,
    minRow: 2,
    maxRow: 7,
    acceptWidgets: true,
    removable: '#trash', // drag-out delete class
    staticGrid: false
  });
  
  GridStack.setupDragIn('.newWidget', { appendTo: 'body', helper: 'clone' });

  let items = [
    // {x: 0, y: 0, w: 2, h: 2, id:user['email']+1, content: '1'},
    // {x: 4, y: 0, w: 4, h: 4, noMove: true, noResize: true, locked: true, content: 'I can\'t be moved or dragged!<br><ion-icon name="ios-lock" style="font-size:300%"></ion-icon>'},
    // {x: 8, y: 0, w: 2, h: 2, id:user['email']+1, content: '<p class="card-text text-center" style="margin-bottom: 0">Drag me!<p class="card-text text-center"style="margin-bottom: 0"><ion-icon name="hand" style="font-size: 300%"></ion-icon><p class="card-text text-center" style="margin-bottom: 0">...but don\'t resize me!'},
    // {x: 10, y: 0, w: 2, h: 2, content: '4'},
    // {x: 0, y: 2, w: 2, h: 2, content: '5'},
    // {x: 2, y: 2, w: 2, h: 4, content: '6'},
    // {x: 8, y: 2, w: 4, h: 2, content: '7'},
    // {x: 0, y: 4, w: 2, h: 2, content: '8'},
    // {x: 4, y: 4, w: 4, h: 2, content: '9'},
    // {x: 8, y: 4, w: 2, h: 2, content: '10'},
    // {x: 10, y: 4, w: 2, h: 2, content: '11'},
  ];
  grid.load(items);
  
  grid.on('added', function(e, items) {
    let str = '';
    items.forEach(function(item) { str += 'id=' + item.id + ' (x,y,w,h)=' + item.x + ',' + item.y + ',' + item.w + ',' + item.h + ' isi=' + item.content; });
    console.log(e.type + ' menambah' + ' ' + items.length + ' items:' + str );
  });

  grid.on('change', function(e, items) {
    let str = '';
    items.forEach(function(item) { str += ' (x,y,w,h)=' + item.x + ',' + item.y + ',' + item.w + ',' + item.h + ' isi=' + item.content; });
    console.log(e.type + ' mengubah' + ' ' + items.length + ' items:' + str );
  });

  grid.on('removed', function(e, items) {
    let str = '';
    items.forEach(function(item) { str += ' (x,y,w,h)=' + item.x + ',' + item.y + ',' + item.w + ',' + item.h + ' isi=' + item.content; });
    console.log(e.type + ' menghapus' + ' ' + items.length + ' items:' + str );
  });

  function save(content = true, full = true) {
    options = grid.save(content, full);
    // console.log(options);
    console.log(JSON.stringify(options));
    // alert ("ok save");
    $.ajax({
         type:'POST',
         url: "{{ route('resources.store') }}",
         data:{message:options},
         success:function(data){
            alert('Berhasil Simpan');
            console.log(options);
            console.log(data);
         }
      });
  }
</script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <!-- support for IE -->
    <style type="text/css">
    .grid-stack-item-removing {
      opacity: 0.8;
      filter: blur(5px);
    }
    #trash {
      background: rgba(255, 0, 0, 0.4);
    }
  </style>
  
@endsection
