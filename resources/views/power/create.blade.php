@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Log Water Consumption</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('water.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="usage" class="col-md-4 col-form-label text-md-right">Usage</label>

                            <div class="col-md-6">
                                <input id="usage" type="text" class="form-control" name="usage" value="{{ old('usage') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="location" class="col-md-4 col-form-label text-md-right">Location</label>

                            <div class="col-md-6">
                                <input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sender" class="col-md-4 col-form-label text-md-right">Sender</label>

                            <div class="col-md-6">
                                <input id="sender" type="text" class="form-control" name="sender" value="{{ old('sender') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="response" class="col-md-4 col-form-label text-md-right">Response</label>

                            <div class="col-md-6">
                                <input id="response" type="text" class="form-control" name="response" value="{{ old('response') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Tambah Log') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
