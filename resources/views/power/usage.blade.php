@extends('layouts.dashboard')
@section('content')



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Node Monitor
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">

        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Small boxes (Stat box) -->
      <!-- <section class="content">
      <div class="row">
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Interactive Area Chart</h3>


            </div>
            <div class="box-body">
            <div id="chart"></div>
          </div>
          </div>

        </div> -->
      <div class="row">

<div class="box">

          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
            <canvas id="myChart" height="100"></canvas>
            {{-- <div class="chart" id="bar-chart" style="height: 300px;"></div> --}}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
            <!-- /.box-header -->
            <div class="box-body">
                {{-- @foreach ($powers as $power)
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-purple"><i class="fa fa-code-fork"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Node {{$power['id_node']}}</span>
                      <span class="info-box-text">R:{{$power['fasa_r']}} S:{{$power['fasa_s']}} T:{{$power['fasa_t']}}</span>
                      <span class="info-box-number">Tiga Fasa (KWH):{{$power['tiga_fasa']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>

                @endforeach --}}
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id Node</th>
                  <th>Id Message</th>
                  <th>Fasa R</th>
                  <th>Fasa S</th>
                  <th>Fasa T</th>
                  <th>3 Fasa</th>
                  <th>Time</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($powers as $power)
                <tr>
                  <td>{{$power['id_node']}}</td>
                  <td>{{$power['id_message']}}</td>
                  <td>{{$power['fasa_r']}}</td>
                  <td>{{$power['fasa_s']}}</td>
                  <td>{{$power['fasa_t']}}</td>
                  <td>{{$power['tiga_fasa']}}</td>
                  <td>{{$power['created_at']}}</td>
                </tr>
                @endforeach

                </tbody>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2021 <a href="#">Jurusan Teknik Elektro</a>
  </footer>

  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script src="{{asset('js/plotly.js')}}"></script>
<script>
	TESTER = document.getElementById('tester');
	Plotly.newPlot( TESTER, [{
	x: [1, 2, 3, 4, 5],
	y: [1, 2, 4, 8, 16] }], {
	margin: { t: 0 } } );
</script>
<script>
function getData() {
  return Math.random();
}

Plotly.plot('chart',[{
  y:[getData()],
  type:'line'
}]);

var cnt = 0;

setInterval(function(){
  Plotly.extendTraces('chart',{y:[[getData()]]}, [0])
  cnt++;

  if(cnt>50){
    Plotly.relayout('chart',{
      xaxis: {
        range: [cnt-50,cnt]
      }
    });
  }
},200);

</script>
<script>
    $(function () {
      "use strict";
      //BAR CHART
      var bar = new Morris.Bar({
        element: 'bar-chart',
        resize: true,
        data: [
          {y: '2006', a: 100},
          {y: '2007', a: 75},
          {y: '2008', a: 50},
          {y: '2009', a: 75},
          {y: '2010', a: 50},
          {y: '2011', a: 75},
          {y: '2012', a: 100}
        ],
        barColors: ['#00a65a'],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['CPU'],
        hideHover: 'auto'
      });
    });
  </script>
          <script>
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: <?php echo $graph_idnode; ?>,
                    datasets: [{
                            label: 'Pemakaian KWH',
                            data: <?php echo $graph_prst; ?>,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>
@endsection
