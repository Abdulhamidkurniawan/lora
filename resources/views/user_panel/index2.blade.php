@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-8 offset-md-2">

            @foreach ($users as $user)
            <div class="card">
                <div class="card-header">

                    {{$user->name}} - {{$user->created_at->diffForHumans()}}

                    <div class="float-right">

                        <form action="{{ route('user_panel.destroy', $user) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }} <!-- membuat delete PostController bisa dibaca -->
                                <a href="{{ route('user_panel.edit', $user) }}" class="btn btn-sm btn-primary">Edit</a>
                                <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    <p> {{str_limit($user->email, 100,'...')}} </p>
                </div>
            </div>
            <br>
            @endforeach
            <br>
            {!! $users->render() !!}
        </div>
</div>
@endsection
