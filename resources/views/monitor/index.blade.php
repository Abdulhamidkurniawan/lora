@extends('layouts.dashboard')
@section('content')



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table monitor Consumption</h3>
            </div>

            <div class="box-body">
            <a href="{{ route('monitor.create') }}" class="btn btn-sm btn-primary">Tambah data</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Mode</th>
                <th>Valve1</th>
                <th>Valve2</th>
                <th>Valve3</th>
                <th>Valve4</th>
                <th>Valve5</th>
                <th>Valve6</th>
                  <!-- <th>Control</th>
                  <th>Updated</th>
                  <th>Aksi</th> -->
                </tr>
                </thead>
                <tbody>
                @foreach ($monitors as $monitor)
                <tr>
                  <!-- <td>{{$monitor->id_node}}</td> -->
                  <td>
                    <form class="" action="{{ route('monitor.update', $monitor)}}" method="post">
                      {{ csrf_field() }}
                      {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                      @if ($monitor->mode == "1")
                      <input id="mode" type="hidden" name="mode" value="0">
                      <button type="submit" class="btn btn-danger" onclick="return confirm('Aktifkan mode manual?')">Manual</button>
                      @else
                      <input id="mode" type="hidden" name="mode" value="1">
                      <button type="submit" class="btn btn-success"onclick="return confirm('Aktifkan mode otomatis?')">Automatic</button>
                      @endif
                    </form>
                    </td>
                  <td>
                  <form class="" action="{{ route('monitor.update', $monitor)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                    @if ($monitor->valve1 == "1")
                    <input id="valve1" type="hidden" name="valve1" value="0">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Matikan valve?')">Matikan Valve</button>
                    @else
                    <input id="valve1" type="hidden" name="valve1" value="1">
                    <button type="submit" class="btn btn-success"onclick="return confirm('Nyalakan valve?')">Nyalakan Valve</button>
                    @endif
                  </form>
                    </td>
                    <td>
                    <form class="" action="{{ route('monitor.update', $monitor)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                    @if ($monitor->valve2 == "1")
                    <input id="valve2" type="hidden" name="valve2" value="0">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Matikan valve?')">Matikan Valve</button>
                    @else
                    <input id="valve2" type="hidden" name="valve2" value="1">
                    <button type="submit" class="btn btn-success"onclick="return confirm('Nyalakan valve?')">Nyalakan Valve</button>
                    @endif
                  </form>
                    </td>
                    <td>
                    <form class="" action="{{ route('monitor.update', $monitor)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                    @if ($monitor->valve3 == "1")
                    <input id="valve3" type="hidden" name="valve3" value="0">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Matikan valve?')">Matikan Valve</button>
                    @else
                    <input id="valve3" type="hidden" name="valve3" value="1">
                    <button type="submit" class="btn btn-success"onclick="return confirm('Nyalakan valve?')">Nyalakan Valve</button>
                    @endif
                  </form>
                    </td>
                    <td>
                    <form class="" action="{{ route('monitor.update', $monitor)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                    @if ($monitor->valve4 == "1")
                    <input id="valve4" type="hidden" name="valve4" value="0">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Matikan valve?')">Matikan Valve</button>
                    @else
                    <input id="valve4" type="hidden" name="valve4" value="1">
                    <button type="submit" class="btn btn-success"onclick="return confirm('Nyalakan valve?')">Nyalakan Valve</button>
                    @endif
                  </form>
                    </td>
                    <td>
                    <form class="" action="{{ route('monitor.update', $monitor)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                    @if ($monitor->valve5 == "1")
                    <input id="valve5" type="hidden" name="valve5" value="0">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Matikan valve?')">Matikan Valve</button>
                    @else
                    <input id="valve5" type="hidden" name="valve5" value="1">
                    <button type="submit" class="btn btn-success"onclick="return confirm('Nyalakan valve?')">Nyalakan Valve</button>
                    @endif
                  </form>
                    </td>
                    <td>
                    <form class="" action="{{ route('monitor.update', $monitor)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                    @if ($monitor->valve6 == "1")
                    <input id="valve6" type="hidden" name="valve6" value="0">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Matikan valve?')">Matikan Valve</button>
                    @else
                    <input id="valve6" type="hidden" name="valve6" value="1">
                    <button type="submit" class="btn btn-success"onclick="return confirm('Nyalakan valve?')">Nyalakan Valve</button>
                    @endif
                  </form>
                    </td>
                  <!-- <td>
                    @if ($monitor->valve == "1")
                    <a href="{{ route('monitor.controloff', $monitor) }}" class="btn btn-sm btn-danger" onclick="return confirm('Matikan valve?')">Matikan Valve</a>
                    @else
                    <a href="{{ route('monitor.controlon', $monitor) }}" class="btn btn-sm btn-success" onclick="return confirm('Nyalakan valve?')">Nyalakan Valve</a>
                    @endif
                    </td>
                  <td>{{$monitor->updated_at->diffForHumans()}}</td>
                  <td><form action="{{ route('monitor.destroy', $monitor) }}" method="post">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <a href="{{ route('monitor.edit', $monitor) }}" class="btn btn-sm btn-primary">Edit</a>
                  <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?')">Hapus</button>
                  </form>

                  </td> -->
                </tr>
                @endforeach

                </tbody>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2021 <a href="#">Jurusan Teknik Elektro</a>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark" style="display: none;">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-water bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the monitor to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endsection
