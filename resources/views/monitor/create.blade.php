@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Node</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('monitor.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="id_node" class="col-md-4 col-form-label text-md-right">Id Node</label>

                            <div class="col-md-6">
                                <input id="id_node" type="text" class="form-control" name="id_node" value="{{ old('id_node') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="valve" class="col-md-4 col-form-label text-md-right">Valve</label>

                            <div class="col-md-6">
                                <input id="valve" type="text" class="form-control" name="valve" value="{{ old('valve') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Tambah Node') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
