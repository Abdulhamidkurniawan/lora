<?php

namespace App\Http\Controllers;

use App\Power;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class PowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nodes = DB::select("SELECT * FROM powers WHERE id IN (
            SELECT MAX(id)
            FROM powers
            GROUP BY id_node
        )ORDER BY id_node ASC");

        $powers = Power::latest()->get();
        return view('power.index', compact('powers','nodes')); /* kirim var */
    }

    public function cari(Request $request)
    {
        $nodes = DB::select("SELECT * FROM powers WHERE id IN (
            SELECT MAX(id)
            FROM powers
            GROUP BY id_node
        )ORDER BY id_node ASC");
        if (empty($request->node)) {
            $powers = Power::latest()->get();}
        else {$powers = Power::latest()->where('id_node','LIKE', "%$request->node%")->get();}
        // dd($request->node,$powers);
        return view('power.index', compact('powers','nodes')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Power  $power
     * @return \Illuminate\Http\Response
     */
    public function show(Power $power)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Power  $power
     * @return \Illuminate\Http\Response
     */
    public function edit(Power $power)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Power  $power
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Power $power)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Power  $power
     * @return \Illuminate\Http\Response
     */
    public function destroy(Power $power)
    {
        $power->delete();

        return redirect()->route('power.index')->withDanger('data berhasil dihapus');
    }

    public function dashboard()
    {
        $powers = Power::latest()->get();
        $valp = $powers->count();
        $nodes = DB::select("SELECT * FROM powers WHERE id IN (
            SELECT MAX(id)
            FROM powers
            GROUP BY id_node
        )ORDER BY id_node ASC");
        // $nodes = json_decode( json_encode($powers), true);
        // dd(date(("d-m-Y H:i:s")));
        $nodep = count($nodes);
        return view('dashboard.power_dashboard', compact('valp','nodep','nodes'));
    }

    public function node()
    {
        // $powers = Power::latest()->get();
        $powers = DB::select("SELECT * FROM powers WHERE id IN (
            SELECT MAX(id)
            FROM powers
            GROUP BY id_node
        )ORDER BY id_node ASC");
        // $barangs = array($barangs);
        // $graph = array($powers);
        if (count($powers) > 0){
        foreach ($powers as $idnode){
            $graph_idnode[]=$idnode->id_node;
        }
        foreach ($powers as $prst){
            $graph_prst[]=$prst->tiga_fasa;
        }
        $graph_idnode = json_encode($graph_idnode);
        $graph_prst = json_encode($graph_prst);}

        else{
        $powers = Power::latest()->get();
        $graph_idnode=$powers;
        $graph_prst=$powers;
        }
        // dd($graph_idnode,$graph_prst);
        $powers= json_decode( json_encode($powers), true);
        return view('power.node', compact('powers','graph_idnode','graph_prst')); /* kirim var */
    }

    public function usage()
    {
        $powers = Power::latest()->limit(100)->get();
        if (count($powers) > 0){
        foreach ($powers as $idnode){
            $graph_idnode[]=$idnode->id_node;
        }
        foreach ($powers as $prst){
            $graph_prst[]=$prst->tiga_fasa;
        }
        // $graph_idnode=$powers;
        // $graph_prst=$powers;
        $graph_idnode = json_encode($graph_idnode);
        $graph_prst = json_encode($graph_prst);
        }
        else{
        return "Tidak ada data";
        }
        // dd($powers,$graph_idnode,$graph_prst);
        $powers= json_decode( json_encode($powers), true);
        return view('power.usage', compact('powers','graph_idnode','graph_prst')); /* kirim var */
    }

    public function usage_report(Request $request)
    {
        $powers = Power::where('id_node','LIKE', "%$request->node%")
        ->whereBetween('created_at', [$request->dari." 00:00:00",$request->sampai." 23:59:59"])->get();
        $usages = Power::latest()->where('id_node','LIKE', "%$request->node%")
        ->whereBetween('created_at', [$request->dari." 00:00:00",$request->sampai." 23:59:59"])->limit(1)->get();
        // dd([$request->dari,$request->sampai],$powers);
        if (count($powers) > 0){
            foreach ($powers as $idnode){
                $graph_idnode[]=$idnode->created_at->format('d-M');
            }
            foreach ($powers as $prst){
                $graph_prst[]=$prst->tiga_fasa;
            }
            $graph_idnode = json_encode($graph_idnode);
            $graph_prst = json_encode($graph_prst);
            }
            else{
            return "Tidak ada data";
            }
            foreach ($usages as $usage){
                $usage=$usage->tiga_fasa;
            }
        // dd($request->dari,$request->sampai,$powers,$graph_idnode,$graph_prst,$usage);
        // dd($powers);
        $powers= json_decode( json_encode($powers), true);
        return view('power.usage', compact('powers','graph_idnode','graph_prst','usage')); /* kirim var */
    }

    public function report(Request $request)
    {
        $mulai = Carbon::parse($request->dari);
        $sampai = Carbon::parse($request->sampai);
        $start = $mulai->startOfMonth()->format('Y-m-d H:i:s'); // 2000-02-01 00:00:00
        $end = $sampai->endOfMonth()->format('Y-m-d H:i:s'); // 2000-02-29 23:59:59
        $loop_bulan = CarbonPeriod::create($mulai, '1 month', $sampai);
        $usage[]='';
        $bulan[]='';
        $loop_mulai[]='';
        $loop_sampai[]='';
        $i=0;
        foreach ($loop_bulan as $dt) {
            $loop_mulai[$i] = Carbon::parse($dt)->startOfMonth()->format('Y-m-d H:i:s');
            $loop_sampai[$i] = Carbon::parse($dt)->endOfMonth()->format('Y-m-d H:i:s');

            $loop_powers = Power::where('id_node','LIKE', "%$request->node%")
            ->where('created_at', '>=', $loop_mulai[$i])
            ->where('created_at', '<=', $loop_sampai[$i])->get();
            if(count($loop_powers)>0){
                $awal = $loop_powers->first();
                $akhir = $loop_powers->last();
                $usage[$i]=$akhir->tiga_fasa-$awal->tiga_fasa;
                $bulan[$i]=Carbon::parse($dt)->format('F');
                // echo $i,'-', $usage[$i],'_',$bulan[$i],'#';
                $usages[$i]=Carbon::parse($dt)->format('F');
                // $usages[$i]=$usages[$i].":".$usage[$i];
                $usages[$i] = array('id_node' => $request->node,'bulan' => $bulan[$i],'usage' => $usage[$i]);
                $i++;
            }

        }
        $powers = Power::where('id_node','LIKE', "%$request->node%")
        ->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)->get();
        // $awal = $powers->first();
        // $akhir = $powers->last();
        // dd($request->dari,$request->sampai,$mulai,$sampai,$loop_bulan,$request->node,$powers,$awal->tiga_fasa,$akhir->tiga_fasa,$usage,$bulan);
        // dd([$request->dari,$request->sampai],$powers);
        if (count($powers) > 0){
            $graph_idnode=$bulan;
            $graph_prst=$usage;
            $graph_idnode = json_encode($graph_idnode);
            $graph_prst = json_encode($graph_prst);
            }
            else{
            return "Tidak ada data report";
            }
        // dd($request->dari,$request->sampai,$graph_idnode,$graph_prst,$usage);
        $powers= json_decode( json_encode($powers), true);
        // dd($tes,$usages,$bulan,$usage,$powers);
        return view('power.report', compact('powers','graph_idnode','graph_prst','usages')); /* kirim var */
    }

    public function phase(Request $request)
    {
        $powers = Power::where('id_node','LIKE', "%$request->node%")
        ->whereBetween('created_at', [$request->dari." 00:00:00",$request->sampai." 23:59:59"])->get();
        $usages = Power::latest()->where('id_node','LIKE', "%$request->node%")
        ->whereBetween('created_at', [$request->dari." 00:00:00",$request->sampai." 23:59:59"])->limit(1)->get();
        // dd([$request->dari,$request->sampai],$powers);
        if (count($powers) > 0){
            foreach ($powers as $idnode){
                $graph_idnode[]=$idnode->created_at->format('d-M');
            }
            foreach ($powers as $prst){
                $graph_r[]=$prst->fasa_r;
                $graph_s[]=$prst->fasa_s;
                $graph_t[]=$prst->fasa_t;
            }
            $graph_idnode = json_encode($graph_idnode);
            $graph_r = json_encode($graph_r);
            $graph_s = json_encode($graph_s);
            $graph_t = json_encode($graph_t);
            }
            else{
            return "Tidak ada data";
            }
        // dd($request->dari,$request->sampai,$powers,$graph_idnode,$graph_prst,$usage);
        // dd($powers);
        $powers= json_decode( json_encode($powers), true);
        return view('power.phase', compact('powers','graph_idnode','graph_r','graph_s','graph_t')); /* kirim var */
    }
}
