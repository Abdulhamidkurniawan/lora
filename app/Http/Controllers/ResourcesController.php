<?php

namespace App\Http\Controllers;

use App\Resources;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grids = Resources::where('user','LIKE', Auth::user()->email)
        ->orderBy('created_at', 'DESC')->get();
        if (count($grids) > 0){
            foreach ($grids as $grid){
                $id[]=$grid->id;
                $id_widget[]=$grid->id_widget;
                $user[]=$grid->user;
                $name[]=$grid->name;
                $top[]=$grid->top;
                $left[]=$grid->left;
                $width[]=$grid->width;
                $height[]=$grid->height;
                $order[]=$grid->order;
                $status[]=$grid->status;
                $widget[]=$grid->widget;
                $locked[]=$grid->locked;
                $content[]=$grid->content;
                $data_stream[]=$grid->data_stream;
                $data[]=$grid->data;
                $content[]=$grid->content;
                $created_at[]=$grid->created_at;
                }

            $id = json_encode($id);
            $id_widget = json_encode($id_widget);
            $user = json_encode($user);
            $name = json_encode($name);
            $top = json_encode($top);
            $left = json_encode($left);
            $width = json_encode($width);
            $height = json_encode($height);
            $order = json_encode($order);
            $status = json_encode($status);
            $widget = json_encode($widget);
            $locked = json_encode($locked);
            $content = json_encode($content);
            $data_stream = json_encode($data_stream);
            $data = json_encode($data);
            $content = json_encode($content);
            $created_at = json_encode($created_at);
            // dd($user);
            }


        $grids= json_decode( json_encode($grids), true);

        return view('resources.monitor');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    // dd($request->All()); 
    for ($i=0;$i <count($request->message);$i++) {
        $terima [] = $request->message [$i];
        $data [] = [
            'id_widget' => Str::uuid(),
            'user' => Auth::user()->email,
            'name' => Auth::user()->name,
            'left' => $terima[$i]['x'],
            'top' => $terima[$i]['y'],
            'width' => $terima[$i]['w'],
            'height' => $terima[$i]['h'],
            'content' => $terima[$i]['content'],
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ];
    // Resources::create([
    //     'user' => Auth::user()->name,
    //     'left' => $data->x,
    //     'top' => $data->y,
    //     'left' => request('x'),
    //     'width' => request('w'),
    //     'height' => request('h'),
    // ]);
    Resources::updateOrCreate(
        ['user' => Auth::user()->email, 'id_widget' => Str::uuid()],
        [
        'id_widget' => Str::uuid(),
        'name' => Auth::user()->name,
        'left' => $terima[$i]['x'],
        'top' => $terima[$i]['y'],
        'width' => $terima[$i]['w'],
        'height' => $terima[$i]['h'],
        'content' => $terima[$i]['content']
    ]);
    }
    // Resources::insert($data);
    return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resources  $resources
     * @return \Illuminate\Http\Response
     */
    public function show(Resources $resources)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resources  $resources
     * @return \Illuminate\Http\Response
     */
    public function edit(Resources $resources)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resources  $resources
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resources $resources)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resources  $resources
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resources $resources)
    {
        //
    }

    public function setting()
    {
        return view('resources.grid');
    }
}
