<?php

namespace App\Http\Controllers;

use App\Monitor;
use Illuminate\Http\Request;

class MonitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monitors = Monitor::latest()->get();

        return view('monitor.index', compact('monitors')); /* kirim var */    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('monitor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Monitor::create([
            'id_node' => request('id_node'),
            'valve' => request('valve'),
        ]);
        return redirect()->route('monitor.index')->withSuccess('data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Monitor  $monitor
     * @return \Illuminate\Http\Response
     */
    public function show(Monitor $monitor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Monitor  $monitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Monitor $monitor)
    {
        return view('monitor.edit', compact('monitor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Monitor  $monitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Monitor $monitor)
    {
        $update = $request->All();
            // dd($update);
        $monitor->update($update);
        return redirect()->route('monitor.index')->withSuccess('data berhasil diubah');        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Monitor  $monitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Monitor $monitor)
    {
        $monitor->delete();

        return redirect()->route('monitor.index')->withDanger('data berhasil dihapus');    }

    public function controlon(Monitor $monitor)
    {
        $monitor->update([
            'valve' => "1",
        ]);
        return redirect()->route('monitor.index')->withInfo('data berhasil diubah');
    }

    public function controloff(Monitor $monitor)
    {
        $monitor->update([
            'valve' => "0",
        ]);
        return redirect()->route('monitor.index')->withInfo('data berhasil diubah');
    }

}
