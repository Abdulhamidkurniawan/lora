<?php

namespace App\Http\Controllers;

use App\Water;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\User;
use App\Debit;
use App\Pump;

class WaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $waters = Water::latest()->get();

        return view('water.index', compact('waters')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('water.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        Water::create([
            'usage' => request('usage'),
            'location' => request('location'),
            'sender' => request('sender'),
            'response' => request('response'),

        ]);
        return redirect()->route('water.index')->withSuccess('data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Water  $water
     * @return \Illuminate\Http\Response
     */
    public function show(Water $water)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Water  $water
     * @return \Illuminate\Http\Response
     */
    public function edit(Water $water)
    {
        // dd($water->usage);
        return view('water.edit', compact('water'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Water  $water
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Water $water)
    {
        // dd($request->all());
        $update = $request->All();
        $water->update($update);
        return redirect()->route('water.index')->withSuccess('data berhasil diubah');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Water  $water
     * @return \Illuminate\Http\Response
     */
    public function destroy(Water $water)
    {
        $water->delete();

        return redirect()->route('water.index')->withDanger('data berhasil dihapus');
    }

    public function get($key)
    {

        echo $key;
        dd($key);
    }

    public function gets(Request $request)
    {   $usage = $request->input('usage');
        $location = $request->input('location');
        $sender = $request->input('sender');
        $response = $request->input('response');
        // $key = Input::get('key');
        echo $usage,$location,$sender,$response;
        // dd($usage,$location,$sender,$response);
        Water::create([
            'usage' => $usage,
            'location' => $location,
            'sender' => $sender,
            'response' => $response,
        ]);
    }

    public function dashboard()
    {
        $waters = Water::latest()->get();
        $valw = $waters->count();
        $debits = Debit::latest()->get();
        $vald = $debits->count();        
        $pumps = Pump::latest()->get();
        $valp = $pumps->count();
        $users = User::latest()->get();
        $valu = $users->count();
        return view('dashboard.water_dashboard', compact('valw','valu','vald','valp'));
    }
}
