<?php

namespace App\Http\Controllers;

use App\Power;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;

class ApiPowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->start and $request->end)){
        $powers = power::whereBetween('created_at', [$request->start." 00:00:00",$request->end." 23:59:59"])
        ->orderBy('id', 'DESC')->get();}

        else{$powers = power::orderBy('id', 'DESC')->get();}
        // dd($powers);

        $powers->map(function($power) {
            $power->time=$power->created_at->shortRelativeDiffForHumans();
            return $power;
        });
        $data = ['status'=>"success",'filter'=>$request->start,'data'=>$powers];
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());
        if(
            $request->id_node == "31" or 
            $request->id_node == "32" or 
            $request->id_node == "33" or 
            $request->id_node == "34" or 
            $request->id_node == "35" or 
            $request->id_node == "36" or 
            $request->id_node == "37" or 
            $request->id_node == "38" or 
            $request->id_node == "39" or 
            strtolower($request->id_node) == "3a"
            ){
                $s1ty=(int)$request->s1ty;
                $s2ty=(int)$request->s2ty;
                $s3ty=(int)$request->s3ty;
                $s4ty=(int)$request->s4ty;
                $s1vm=(int)$request->s1vm;
                $s2vm=(int)$request->s2vm;
                $s3vm=(int)$request->s3vm;
                $s4vm=(int)$request->s4vm;
                $s1vl=(int)$request->s1vl;
                $s2vl=(int)$request->s2vl;
                $s3vl=(int)$request->s3vl;
                $s4vl=(int)$request->s4vl;

                $power=new Power();
                $power->id_node=$request->id_node;
                $power->id_message=$request->id_message;
                $power->fasa_r=($s1vl+($s1vm*255));
                $power->fasa_s=($s2vl+($s2vm*255));
                $power->fasa_t=($s3vl+($s3vm*255));
                $power->tiga_fasa=($s4vl+($s4vm*255));
                $power->save();

                $data = ['status'=>"success"];
                return $data;
            }
        else {
            $data = ['status'=>"failed-Node tidak dikenali"];
            return $data;
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $power = Power::find($id);
        return $power;    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $power = Power::find($request->id);
        $power->id_node=$request->id_node;
        $power->id_message=$request->id_message;
        $power->fasa_r=(int)$request->fasa_r;
        $power->fasa_s=(int)$request->fasa_s;
        $power->fasa_t=(int)$request->fasa_t;
        $power->tiga_fasa=(int)$request->tiga_fasa;
        $power->save();
        $data = ['status'=>"success"];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $power = Power::find($id);
        $power->delete();
        $data = ['status'=>"success"];
        return $data;
    }

    public function node()
    {
        $powers = DB::select("SELECT * FROM powers WHERE id IN (
            SELECT MAX(id)
            FROM powers
            GROUP BY id_node
        )ORDER BY id_node ASC");
        // $powers= json_decode( json_encode($powers), true);
        // dd($powers);
        // foreach ($powers as $power){
        // $power['time'] = Carbon::parse($power['created_at'])->shortRelativeDiffForHumans();
        $collection = collect($powers);
        // dd($collection);
        $collection->map(function($power) {
            $power->time=Carbon::parse($power->created_at)->shortRelativeDiffForHumans();
            return $power;
        });
        $data = ['status'=>"success",'data'=>$powers];
        return $data;
    }

    public function data_node($id, Request $request)
    {
        if (!empty($request->start and $request->end)){
        $powers = Power::where('id_node','LIKE', "%$id%")
        ->whereBetween('created_at', [$request->start." 00:00:00",$request->end." 23:59:59"])
        ->orderBy('id', 'DESC')->get();}

        else {$powers = Power::where('id_node','LIKE', "%$id%")
            ->orderBy('id', 'DESC')->get();}

        $powers->map(function($power) {
            $power->time=$power->created_at->shortRelativeDiffForHumans();
            return $power;
        });
        $data = ['status'=>"success",'node'=>$id,'filter'=>$request->start,'data'=>$powers];
        return $data;
    }
}
