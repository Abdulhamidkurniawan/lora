<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Water;
use App\User;
use App\Debit;
use App\Pump;
use Artisan;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home');
    }

    public function dashboard()
    {
        $waters = Water::latest()->get();
        $valw = $waters->count();
        $debits = Debit::latest()->get();
        $vald = $debits->count();        
        $pumps = Pump::latest()->get();
        $valp = $pumps->count();
        $users = User::latest()->get();
        $valu = $users->count();
        return view('dashboard', compact('valw','valu','vald','valp'));
    }

    public function optimize()
    {
        $exitCode = Artisan::call('optimize');
        return '<h1>Reoptimized class loader</h1>';    
    }
}
