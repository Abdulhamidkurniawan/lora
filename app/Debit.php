<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debit extends Model
{
    protected $fillable = ['debit','location','sender','response']; /* yang bsa di isi */
}
