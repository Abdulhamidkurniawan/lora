<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resources extends Model
{
    protected $fillable = ['id_widget','user','name','top','left','width','height','order','status','widget','locked','content','data_stream','data']; /* yang bsa di isi */
}
