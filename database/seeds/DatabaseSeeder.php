<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'admin',
            'email' => 'admin@gmail.com',
            'jabatan' => 'admin',
            'password' => bcrypt('adadadad')],
            ['name' => 'karyawan',
            'email' => 'karyawan@gmail.com',
            'jabatan' => 'karyawan',
            'password' => bcrypt('adadadad')],
            ['name' => 'user',
            'email' => 'user@gmail.com',
            'jabatan' => 'user',
            'password' => bcrypt('adadadad')],
        ]);

        DB::table('jabatans')->insert([
            ['jabatan' => 'admin'],
            ['jabatan' => 'karyawan'],
            ['jabatan' => 'user'],
        ]);    }
}
