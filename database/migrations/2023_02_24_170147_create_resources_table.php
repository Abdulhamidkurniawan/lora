<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_widget')->nullable();
            $table->string('user')->nullable();
            $table->string('name')->nullable();
            $table->string('top')->nullable();
            $table->string('left')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('order')->nullable();
            $table->string('status')->nullable();
            $table->string('widget')->nullable();
            $table->string('locked')->nullable();
            $table->text('content')->nullable();
            $table->string('data_stream')->nullable();
            $table->string('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
