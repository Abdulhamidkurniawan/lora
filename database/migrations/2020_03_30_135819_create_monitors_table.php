<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_node')->nullable();
            $table->string('mode')->nullable();
            $table->string('valve1')->nullable();
            $table->string('valve2')->nullable();
            $table->string('valve3')->nullable();
            $table->string('valve4')->nullable();
            $table->string('valve5')->nullable();
            $table->string('valve6')->nullable();
            $table->string('valve7')->nullable();
            $table->string('valve8')->nullable();
            $table->string('valve9')->nullable();
            $table->string('valve10')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitors');
    }
}
